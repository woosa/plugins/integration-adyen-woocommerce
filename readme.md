## Requirements

* `nodejs`
* `composer`
* `PHP >=7.1`

## Setup

1. Open `config.js` and define all the information
2. run `npm start` to set up the files
3. run `npm run build` to build the files for production which will be found in `/dist/` folder
4. run `npm run zip_dist` to zip the dist files

## Notes

* use namespace to encapsulate your PHP code
* use comments to describe your code
* think modularly, extendable, and optimized as possible