## 1.13.0 - 2025-02-13

### Added

* New payment method `Twint`
* Support for recurring payments for `PayPal`
* New hook `adn\logger\get_file_dir` for the path where logs are saved

### Fixed

* The error `CRITICAL Uncaught DivisionByZeroError: Division by zero in [..]` when PHP `max_execution_time` is `0`

## 1.12.3 - 2024-12-19

### Changed

* Added hashes to log file names to enhance security

## 1.12.2 - 2024-12-16

### Fixed

* Using a later hook in the core logic causes the filtering of the existing payment methods to fail

## 1.12.1 - 2024-12-12

### Fixed

* Security issue regarding to log files
* The premature triggering of the translation warning
* Excessive log files are being generated during attempts to retrieve Adyen payment methods

## 1.12.0 - 2024-12-05

### Added

* New payment method named `Gift Card`

### Changed

* The payment method `Credit Card` has been renamed to `Card`

### Removed

* The following payment methods are no longer available: `Giropay`, `Sofort`

## 1.11.3 - 2024-11-07

### Changed

* Add suffix for composer autoloader class

### Fixed

* The URL for downloading Apple Pay certificate is broken

## 1.11.2 - 2024-10-10

### Fixed

* Clicking on the Credit Card field `Name` makes the menu dissappears
* Some documentation links are outdated
* In PHP 8.2 there are some warnings about dynamic properties

## 1.11.1 - 2024-09-12

### Changed

* A second parameter has been added for the hook `\rest_api\payload_data`

### Fixed

* Recurring payment is not working properly

## 1.11.0 - 2024-07-18

### Added

* Support for the new WooCommerce Checkout Blocks

### Fixed

* Credit card translation is reseting to EN when the zipcode is filled in
* The error message `The Payment was not successful` is displayed way before the payment actually fails

## 1.10.1 - 2024-06-20

### Fixed

* The Adyen store identification is failing for some shops
* The translation of payment methods is not working properly

## 1.10.0 - 2024-05-23

### Added

* A new settings section called `Stores` allows you to define which Adyen stores to use based on the customer's country

### Fixed

* Refund amount greater then 1000 is not displayed correctly in the order note
* Google pay icon is displayed only with white color

## 1.9.1 - 2024-03-21

### Fixed

* The plugin zip file has been compiled with errors

## 1.9.0 - 2024-03-20

### Added

* New design for the settings page
* New section called Logs which will display debug and error logs
* New setting option for Credit card payment method to set the card holder field required or not
* New hook: `\service_util\manual_payment_methods\list`

### Changed

* No longer offer the plugin for free on wordpress.org

### Fixed

* Validation of HMAC is not working properly
* The option to enable/disable installments is not working properly

## 1.8.0 - 2024-01-23

### Added

* New payment method `Trustly` (only one-time payments)
* New hooks have been added: `adn\core_hook_assets\public_assets`, `adn\validate_fields\require_cardholder_name` , `adn\validate_fields\require_cardholder_name`, `adn\validate_fields\require_cvc`, `adn\rest_api_hook\unexpected_event`, `adn\rest_api\is_authenticated\force_full_authentication`, `adn\rest_api\is_authenticated\log_unauthenticated_attempt`

### Changed

* Return specific response for unauthenticated Adyen webhooks
* For recurring payments the request property `paymentMethod/recurringDetailReference` has been replaced with `paymentMethod/storedPaymentMethodId`

### Fixed

* Don't hide the processing message when performing a redirecting payment
* Too small 3DS authentication pop-up window

## 1.7.0 - 2023-09-28

### Added

* Added support for `HPOS (High-Performance Order Storage)`
* The following hooks have been added: `adn\order\payment_result`, `adn\process_refund\allow_sepa_direct_debit_refunds_after_capture`, `adn\rest_api\payload_data`, `adn\rest_api_hook\authorisation\manual_immediate_capture`, `adn\rest_api_hook\authorisation\payment_completed`, `adn\rest_api_hook\authorisation\payment_failed`, `adn\rest_api_hook\cancellation\success`, `adn\rest_api_hook\cancellation\failure`, `adn\rest_api_hook\capture\payment_completed`. `adn\rest_api_hook\capture\payment_failed`, `adn\rest_api_hook\capture_failed`, `adn\rest_api_hook\refund\sucess`, `adn\rest_api_hook\refund\failure`, `adn\rest_api_hook\refund_failed`, `adn\rest_api_hook\cancel_or_refund\success`, `\rest_api_hook\cancel_or_refund\failure`, `adn\rest_api_hook\recurring_contract`

### Changed

* In case the proxy is not available then use fallback API endpoints

## 1.6.1 - 2023-07-20

### Fixed

* Some payment methods are not properly displayed on the checkout page due to a JS error
* The test mode is not correctly set for Google Pay

## 1.6.0 - 2023-07-13

### Added

* Added the payment method `Online banking Poland` which replaces the old one `DotPay`

### Changed

* The JS library of Adyen Web Component has been upgraded from `v4.8.0` to `5.33.0`
* The Adyen API versions have been ugraded as follow: Checkout from `v67` to `v68` and Recurring from `v49` to `v68`
* The error message "Notifications could not be authenticated.." has been improved to show more details for a better troubleshooting

### Removed

* The payment method `DotPay` has been removed since is replaced by `Online banking Poland`

## 1.5.3 - 2023-06-07

### Fixed

* Fixed the error `PHP Warning: Undefined array key "path"...`

## 1.5.2 - 2023-05-09

### Fixed

* The credit card form slides up and down when the user is typing in the address fields
* For some shops the Adyen webhooks fail due to an internal error

## 1.5.1 - 2023-03-23

### Fixed

* Google pay cannot be enabled due to a change in the method name generated by Adyen
* Correct the typo in the plugin installation steps
* The authorization of webhooks fails if the password has special characters
* The UI of Authorization page is broken

### Changed

* The support chat is now available in the plugin settings

## 1.5.0 - 2023-01-17

### Added

* Added new payment method `Swish`
* Added new payment method `Bancontact mobile`
* Added new payment method `Vipps`
* Added new payment method `MobilePay`
* Added new payment method `MolPay`
* Added new payment method `GrabPay`
* Added support for using HMAC signature for Webhooks

### Changed

* In case an order payment fails the error reason is saved now as note on the order
* The credit card form is expanding now automatiacally if there are not saved cards

### Fixed

* Format the shop languages which contain suffixes like `_formal` or `_informal` to avoid errors in the request to Adyen

## 1.4.2 - 2022-12-22

* [FIX] - The tax calculation does not take into account correctly the WooCommerce tax settings

## 1.4.1 - 2022-11-29

* [FIX] - The Apple Pay token is not generated
* [FIX] - The tax is not taken into account in checkout, this leads to error `Apple Pay token amount-mismatch`
* [FIX] - Cancelled payments doesn't correctly redirect

## 1.4.0 - 2022-06-15

* [FIX] - Allow special characters for cardholder name
* [FIX] - Fix the warning `Warning: Invalid argument supplied for foreach() in /home/users/ivn-dev/public_html/content/plugins/integration-adyen-woocommerce/includes/service/class-service-checkout.php on line 163`
* [FIX] - For products with price 0 the tax is calculated wrongly
* [FIX] - Let WooCommerce to mark the order status as `Completed` for virtual & downloadable products
* [TWEAK] - Stop sending remote requests if the last response is 401 unauthorized
* [TWEAK] - Upgrade Adyen Web component from v4.4.0 to v4.8.0
* [FEATURE] - New setting option for each payment method to define a custom icon

## 1.3.2 - 2022-04-12

* [FIX] - Authorization of the plugin gets failed in some scenarios
* [CHANGE] - Replace Adyen's PHP library with our custom logic
* [TWEAK] - Added step-by-step guide how to configure Apple pay payment method

## 1.3.1 - 2022-03-15

* [FIX] - The price tax is not calculated correctly for line items


## 1.3.0 - 2022-02-09

* [FEATURE] - Added new payment method Dotpay
* [FEATURE] - Added new payment method BLIK
* [FEATURE] - Added new payment method Apple Pay
* [IMPROVEMENT] - Make more logic consistency between all payment methods
* [TWEAK] - The "Notification" settings section has been renamed to "Webhooks" to be consistent with Adyen
* [TWEAK] - The recurring reference is now used from the shop subscription instead to be retrieved from Adyen over and over again
* [TWEAK] - When a customer removes its personal data a note will be created on the order

## 1.2.1 - 2022-01-11

* [FIX] - Credit card form error in Javascript which affected the holder name field
* [FIX] - Use the first payment reference from the parent order instead of the subscription
* [FIX] - Add extra checks for Bancontact to make sure it gets the correct recurring reference

## 1.2.0 - 2021-08-05

* [FIX] - Wrong config for Google Pay
* [FIX] - Failed/canceled payments redirect to thank you page instead of the pay order page
* [FIX] - Installments are not displayed for credit card
* [FIX] - Wrong supported countries for Klarna
* [FEATURE] - Added recurring payments support for Klarna payment method

## 1.1.4 - 2021-05-27

* [FIX] - Fix Javascript conflict on License section

## 1.1.3 - 2021-05-13

* [FIX] - Sending payment details fails in some cases due to cookie
* [FIX] - Undefined function in Paypal method
* [FIX] - Change Google pay icon
* [IMPROVEMENT] - Upgrade Adyen PHP client library from v5.0.0 to v10.1.0
* [IMPROVEMENT] - Upgrade Adyen JS component from v3.12.1 to v4.4.0
* [TWEAK] - Small changes to Tools section


## 1.1.2 - 2021-04-12

* [IMPROVEMENT] - Rebuilt license management and the logic of receving updates

## 1.1.1 - 2020-10.03

* [FIX] - Fixed missing icon on "Use new card" button in checkout page
* [FIX] - Stored cards are not removed properly from the general cache and this gives conflicts for guest users
* [FIX] - Fixed wrong variable name "$s_address"
* [FIX] - Integration of Giropay payment method is changed to "API-only" mode because it does not work properly via JS component
* [FIX] - Mount the JS component of credit card form only once to avoid multiple unnecessary calls
* [FIX] - Inform the admin by a warning message when the domain key must be manually re-generated

## 1.1.0 - 2020-09.09

* [FIX] - Added a new setting option to define a "Reference Prefix" to avoid conflicts in processing orders on multisite installation
* [FEATURE] - Added PayPal payment method (no recurring payments supported yet)
* [FEATURE] - Added Klarna payment method (no recurring payments supported yet)
* [FEATURE] - Added a new option to allow customers to remove their payment personal data according to GDPR
* [FEATURE] - Added recurring payments support for Google Pay payment method
* [TWEAK] - Added more data in the API requests to avoid fraud detection
* [TWEAK] - Display supported countries and currencies on each payment method
* [TWEAK] - Added a new settings section called "Tools"

## 1.0.10 - 2020-08.13

* [FIX] - Added support for Bancontact payment method to be used with subscrptions as well
* [FIX] - Exclude stored credit cards from the general cache
* [FIX] - Fixed broken design of credit cards form in the checkout page
* [FIX] - Fixed JS scripts issue in Wordpress 5.5

## 1.0.9 - 2020-07.23

* [FIX] - Added support for variable subscription products to avoid the "pending payment" order status
* [FIX] - Made house number optional when paying with credit card
* [FIX] - Fixed the empty user reference when paying with a saved credit card
* [FIX] - Encapsulated the entire code to avoid conflicts with other plugins which use the same dependency libraries

## 1.0.8 - 2020-06-29

* [FIX] - The origin key was not regenerated on saving settings action

## 1.0.7 - 2020-06-25

* [FIX] - Fixed wrong reference for recurring orders
* [FIX] - Cache the available payment methods to increase the speed time
* [FIX] - Fixed conflicts for generating the origin keys
* [TWEAK] - Rearranged the settings page and made it accessible even if the license is inactive

## 1.0.6 - 2020-05-01

* [FIX] - Fixed the problem of removing stored cards from "My account" page
* [FIX] - Fixed wrong payment url for subscription products
* [FIX] - Fixed credit cards conflict for guest users
* [TWEAK] - Do not regenerate the credit card form when the checkout contents reload
* [TWEAK] - Add extra info (billing address) in the payment request

## 1.0.5 - 2020-02-19

* [FIX] - Credit card form is not loading due to `origin keys` is not generated
* [FIX] - Credit card form multiple click events conflict
* [FEATURE] - New option to set whether or not to remove plugin data on uninstall
* [TWEAK] - Rearrange settings sub-tabs
* [TWEAK] - Add falback for JS file dependencies

## 1.0.4 - 2020-01-09

* [FEATURE] - Added Google Pay payment method
* [FEATURE] - Added Wechat Pay payment method
* [TWEAK] - Show authentication  status
* [TWEAK] - Add caching for some API requests and a button to clear this cache

## 1.0.3 - 2019-11-26

* [FEATURE] - Boleto payment method added
* [FEATURE] - Alipay payment method added
* [FEATURE] - Card installments support added
* [FEATURE] - New option to capture payments manually
* [FEATURE] - New option to save credit cards for future payments
* [FEATURE] - New section in My Account page to display the saved credit cards

## 1.0.2 - 2019-09-25

* [FIX] - Fixed missing function for getting subscriptions

## 1.0.1 - 2019-09-20

* [FIX] - Fixed activation plugin issue

## 1.0.0 - 2019-09-05

* This is the first release, yeey!