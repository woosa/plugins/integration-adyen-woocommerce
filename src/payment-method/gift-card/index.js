import Label from "../../components/Label";
import WebComponentGeneric from '../../components/WebComponentGeneric';

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_gift_card_data', {
   icon: false,
} );

const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - Gift Card', 'integration-adyen-woocommerce' );

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <WebComponentGeneric
         paymentMethod={'giftcard'}
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
         generatePaymentMethodData={(data) => ({
            "adn_giftcard_brand": data.paymentMethod ? data.paymentMethod.brand : '',
            "adn_giftcard_number": data.paymentMethod ? data.paymentMethod.encryptedCardNumber : '',
            "adn_giftcard_cvc": data.paymentMethod ? data.paymentMethod.encryptedSecurityCode : '',
         })}
      />
   </>;
};

const config = {
   name: 'woosa_adyen_gift_card',
   label: <Label text={title} image={settings.icon} name={'woosa_adyen_gift_card'} />,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};

window.wc.wcBlocksRegistry.registerPaymentMethod( config );