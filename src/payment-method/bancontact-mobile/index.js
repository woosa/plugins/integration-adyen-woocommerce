import Label from "../../components/Label";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_bancontact_mobile_data', {
   icon: false
} );

const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - Bancontact mobile', 'integration-adyen-woocommerce' );

const Content = () => {
   return window.wp.htmlEntities.decodeEntities( settings.description || '' );
};

const config = {
   name: 'woosa_adyen_bancontact_mobile',
   label: <Label text={title} image={settings.icon} name={'woosa_adyen_bancontact_mobile'} />,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};

window.wc.wcBlocksRegistry.registerPaymentMethod( config );