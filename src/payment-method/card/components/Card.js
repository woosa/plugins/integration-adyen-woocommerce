import { useEffect, useState } from 'react';
import AdyenCheckoutInstance from '../../../util/AdyenCheckoutInstance'

export default function Card({card, index, setEncryptedCardData, setActiveIndex, activeIndex, setIsStoredCard}) {

   const [show, setShow] = useState(false);
   const [isInitCard, setIsInitCard] = useState(false);


   useEffect(() => {

      if (isInitCard) {

         AdyenCheckoutInstance(setEncryptedCardData).then(function(response){

            const storedMethods = response.paymentMethodsResponse.storedPaymentMethods;

            response.create('card', storedMethods[index]).mount('#adn-scheme-card-'+index);

            setShow(true);

            setIsStoredCard(true);

         });

         setIsInitCard(false);


      }

   });

   function initCard() {

      setActiveIndex(index);

      setIsInitCard(true);

   }

   function isShow() {

      if (-1 === activeIndex) {
         return false;
      }

      return show;

   }

   return (

      <div className="adn-stored-card is-stored-card" onClick={initCard}>
         <div className="adn-stored-card__details" data-adn-stored-card={'adn-scheme-card-' + index} data-adn-stored-card-type="scheme">
            <img src={'https://checkoutshopper-test.adyen.com/checkoutshopper/images/logos/' + card['brand'] +'.svg'} alt="" />
            <div>******{card['lastFour']}</div>
         </div>
         <div className="adn-stored-card__fields" style={{display: isShow() ? "block" : "none"}} id={'adn-scheme-card-'+index}></div>
      </div>

   );

}