import { useEffect, useState } from 'react';
import StoredCard from "./StoredCard";

export default function CardForm({ installments, stored_cards, eventRegistration, emitResponse }) {

   const [encryptedCardData, setEncryptedCardData] = useState({
      encryptedCardNumber: '',
      encryptedExpiryMonth: '',
      encryptedExpiryYear: '',
      encryptedSecurityCode: '',
      holderName: '',
      storedPaymentMethodId: '',
      store_card: '',
      installments: ''
   });

   const [isStoredCard, setIsStoredCard] = useState(false);

   const { onPaymentProcessing } = eventRegistration;

   useEffect( () => {
      const unsubscribe = onPaymentProcessing( async () => {
         const customDataIsValid = !! encryptedCardData;

         if ( customDataIsValid ) {
            return {
               type: emitResponse.responseTypes.SUCCESS,
               meta: {
                  paymentMethodData: {
                     "adn-scheme-card-number": encryptedCardData.encryptedCardNumber,
                     "adn-scheme-card-exp-month": encryptedCardData.encryptedExpiryMonth,
                     "adn-scheme-card-exp-year": encryptedCardData.encryptedExpiryYear,
                     "adn-scheme-card-cvc": encryptedCardData.encryptedSecurityCode,
                     "adn-scheme-card-holder": encryptedCardData.holderName,
                     "adn-scheme-sci": encryptedCardData.storedPaymentMethodId,
                     "adn-scheme-store-card": encryptedCardData.store_card,
                     "adn-scheme-is-stored-card": isStoredCard ? "yes" : "no",
                     "adn-scheme-card-installments": encryptedCardData.installments,
                  },
               },
            };
         }

         return {
            type: emitResponse.responseTypes.ERROR,
            message: 'There was an error',
         };
      } );

      return () => {
         unsubscribe();
      };
   }, [
      emitResponse.responseTypes.ERROR,
      emitResponse.responseTypes.SUCCESS,
      onPaymentProcessing,
      encryptedCardData
   ] );

   return (
      <div className="adn-wrap-form">

         <StoredCard setEncryptedCardData={setEncryptedCardData} stored_cards={stored_cards} installments={installments} setIsStoredCard={setIsStoredCard}/>

         <input type="hidden" id="adn-scheme-card-number" name="adn-scheme-card-number" value={encryptedCardData.encryptedCardNumber} />
         <input type="hidden" id="adn-scheme-card-exp-month" name="adn-scheme-card-exp-month" value={encryptedCardData.encryptedExpiryMonth} />
         <input type="hidden" id="adn-scheme-card-exp-year" name="adn-scheme-card-exp-year" value={encryptedCardData.encryptedExpiryYear} />
         <input type="hidden" id="adn-scheme-card-cvc" name="adn-scheme-card-cvc" value={encryptedCardData.encryptedSecurityCode} />

         <input type="hidden" id="adn-scheme-card-holder" name="adn-scheme-card-holder" value={encryptedCardData.holderName} />
         <input type="hidden" id="adn-scheme-sci" name="adn-scheme-sci" value={encryptedCardData.storedPaymentMethodId} />
         <input type="hidden" id="adn-scheme-store-card" name="adn-scheme-store-card"  value={encryptedCardData.store_card}  />
         <input type="hidden" id="adn-scheme-is-stored-card" name="adn-scheme-is-stored-card" value="yes" />
         <input type="hidden" id="adn-scheme-card-installments" name="adn-scheme-card-installments" value={encryptedCardData.store_card} />

         <input type="hidden" data-adn-card-installments="" value={installments} />

      </div>
   );
}