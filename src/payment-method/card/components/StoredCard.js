import { useEffect, useState } from 'react';
import Card from "./Card"
import AdyenCheckoutInstance from '../../../util/AdyenCheckoutInstance'

export default function StoredCard({setEncryptedCardData, stored_cards, installments, setIsStoredCard}) {

   const [show, setShow] = useState(false);
   const [activeIndex, setActiveIndex] = useState(-1);
   const [isInitCard, setIsInitCard] = useState(false);
   const [isWasInit, setIsWasInit] = useState(false);
   const Translation = adn_util.translation;

   useEffect(() => {

      if (!isWasInit && stored_cards && stored_cards.length === 0) {

         initNewCard();

      }

      if (isInitCard) {

         if (isWasInit) {

            setActiveIndex(-1);
            setShow(true);
            setIsStoredCard(false);
            setIsInitCard(false);

         } else {

            initNewCard();

         }

      }

   });

   function initNewCard() {

      let paymentMethodsConfig = '';

      try{

         const card_installments = JSON.parse(installments);

         if(card_installments.constructor === Array){

            paymentMethodsConfig = {
               card: {
                  installmentOptions: {
                     card: {
                        values: card_installments,
                        // Shows regular and revolving as plans shoppers can choose.
                        // plans: [ 'regular', 'revolving' ]
                     },
                  },
                  // Shows payment amount per installment.
                  showInstallmentAmounts: true
               }
            }
         }

      }catch (e){}

      AdyenCheckoutInstance(setEncryptedCardData).then(function(response){

         const woosa = adn_util;

         response.setOptions({
            paymentMethodsConfiguration: paymentMethodsConfig,
         }).create('card', {
            brands: woosa.api.card_types,
            enableStoreDetails: woosa.api.store_card,
            hasHolderName: woosa.api.has_holder_name,
            holderNameRequired: woosa.api.holder_name_required,
         }).mount('#adn-scheme-card-new');

         setActiveIndex(-1);
         setShow(true);
         setIsStoredCard(false);

      });

      setIsInitCard(false);
      setIsWasInit(true);

   }

   function initCard() {

      if (isWasInit && isShow()) {

         setShow(false);

      } else {

         setIsInitCard(true);

      }

   }

   function isShow() {

      if ( activeIndex !== -1 ) {
         return false;
      }

      return show;

   }

   const listStoredCards = stored_cards.map((card, index) =>
      <Card
         card={card}
         index={index}
         key={index}
         setEncryptedCardData={setEncryptedCardData}
         setActiveIndex={setActiveIndex}
         activeIndex={activeIndex}
         setIsStoredCard={setIsStoredCard}
      />
   );

   return (

      <div className="adn-stored-cards">

         {listStoredCards}

         <div className="adn-stored-card" >
            <div className="adn-stored-card__details" data-adn-stored-card="adn-scheme-card-new" data-adn-stored-card-type="scheme" onClick={initCard}>
               <span className="dashicons dashicons-plus"></span>
               <div>{Translation.use_a_new_card}</div>
            </div>
            <div className="adn-stored-card__fields" id="adn-scheme-card-new" style={{display: isShow() ? "block" : "none"}}>
               <div id="adn-card-form"></div>
            </div>
         </div>

      </div>

   );

}