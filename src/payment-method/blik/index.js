import Label from "../../components/Label";
import WebComponentGeneric from '../../components/WebComponentGeneric';
import PaymentAction from "../../components/PaymentAction";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_blik_data', {
   icon: false,
   payment_action: false
} );

const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - BLIK', 'integration-adyen-woocommerce' );

// should be not in content to run for non active payment methods
const LabelPaymentAction = () => {
   return <>
      {(!!settings.payment_action && Object.keys(settings.payment_action).length > 0) &&
         <PaymentAction
            action={settings.payment_action}
         />
      }
   </>;
};

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <WebComponentGeneric
         paymentMethod={'blik'}
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
         generatePaymentMethodData={(data) => ({
            "adn_blik_code": data.paymentMethod ? data.paymentMethod.blikCode : '',
         })}
      />
   </>;
};

const config = {
   name: 'woosa_adyen_blik',
   label: <><Label text={title} image={settings.icon} name={'woosa_adyen_blik'} /><LabelPaymentAction /></>,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};

window.wc.wcBlocksRegistry.registerPaymentMethod( config );