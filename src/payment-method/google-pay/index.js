import Label from "../../components/Label";
import GooglePay from './GooglePay';
import PaymentAction from "../../components/PaymentAction";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_google_pay_data', {
   icon: false,
   payment_action: false
} );

const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - Google Pay', 'integration-adyen-woocommerce' );

// should be not in content to run for non active payment methods
const LabelPaymentAction = () => {
   return <>
      {(!!settings.payment_action && Object.keys(settings.payment_action).length > 0) &&
         <PaymentAction
            action={settings.payment_action}
         />
      }
   </>;
};

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <GooglePay
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
         merchantIdentifier={settings.merchant_identifier}
         testmode={settings.testmode}
         description={settings.google_description}
         token={settings.token}
      />
   </>;
};

const config = {
   name: 'woosa_adyen_google_pay',
   label: <><Label text={title} image={settings.icon} name={'woosa_adyen_google_pay'} /><LabelPaymentAction /></>,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};

window.wc.wcBlocksRegistry.registerPaymentMethod( config );