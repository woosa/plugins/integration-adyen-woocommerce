import { useEffect, useState } from 'react';
import AdyenCheckoutInstance from "../../util/AdyenCheckoutInstance";

export default function CardForm({ eventRegistration, emitResponse,  merchantIdentifier, testmode, description, token }) {

   const [googlePayData, setGooglePayData] = useState({
      description: '',
      token: '',
   });

   const [isWasInit, setIsWasInit] = useState(false);


   const { onPaymentProcessing } = eventRegistration;
   const Translation = adn_util.translation;


   function initAdyen() {

      AdyenCheckoutInstance().then(function(response){
         const woosa = adn_util;

         var component = response.create(adn_checkout.google_method_type, {
            countryCode: woosa.cart.country,
            environment: testmode ? 'TEST' : 'PRODUCTION',
            amount: {
               currency: woosa.currency,
               value: (woosa.cart.total) * 100, //it's in cents
            },
            configuration: {
               gatewayMerchantId: woosa.api.adyen_merchant,
               merchantName: woosa.site_name,
               merchantId: merchantIdentifier
            },
            buttonColor: "default",
            onAuthorized: (data) => {

               setGooglePayData({
                  description: data.paymentMethodData.description,
                  token: data.paymentMethodData.tokenizationData.token
               });
            }
         });

         component.isAvailable().then(() => {

            component.mount("#woosa_adyen_google_pay_button");
            setIsWasInit(true);

         }).catch(e => {
            console.log(e);
         });

      });

   }

   useEffect( () => {
      const unsubscribe = onPaymentProcessing( async () => {
         const customDataIsValid = !! googlePayData.token;

         if ( customDataIsValid ) {

            return {
               type: emitResponse.responseTypes.SUCCESS,
               meta: {
                  paymentMethodData: {
                     ["woosa_adyen_google_pay_token"]: googlePayData.token,
                     ["woosa_adyen_google_pay_is_strip_slashes"]: false,
                  },
               },
            };
         }

         return {
            type: emitResponse.responseTypes.ERROR,
            message: Translation.google_error
         };
      } );


      if (!isWasInit) {
         initAdyen();
      }

      return () => {
         unsubscribe();
      };
   }, [
      emitResponse.responseTypes.ERROR,
      emitResponse.responseTypes.SUCCESS,
      onPaymentProcessing,
      googlePayData
   ] );

   return (

      <div id="adn-googlepay-container">
         <div id="woosa_adyen_google_pay_button"></div>
         {googlePayData.description ? (
            <div
               className="googlepay-description"
               style={{display: "block"}}
            >
               {googlePayData.description}
            </div>
         ) : (
            <div
               className="googlepay-description"
               style={{display: (!!description && !!token) ? "block" : "none"}}
            >
               {description}
            </div>
         )}
      </div>

   );
}