import Label from "../../components/Label";
import ApplePay from './ApplePay';
import PaymentAction from '../../components/PaymentAction';

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_apple_pay_data', {
   icon: false,
   payment_action: false
} );

const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - Apple Pay', 'integration-adyen-woocommerce' );

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <ApplePay
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
      />
      {(!!settings.payment_action && Object.keys(settings.payment_action).length > 0) &&
         <PaymentAction
            action={settings.payment_action}
         />
      }
   </>
   ;
};

const config = {
   name: 'woosa_adyen_apple_pay',
   label: <Label text={title} image={settings.icon} name={'woosa_adyen_apple_pay'} />,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};

window.wc.wcBlocksRegistry.registerPaymentMethod( config );

window.wc.wcBlocksRegistry.registerPaymentMethodExtensionCallbacks(
   'woosa-adyen-for-woocommerce',
   {
      woosa_adyen_apple_pay: (arg) => {
         return !!window.ApplePaySession;
      },
   }
);
