import { useEffect, useState } from 'react';

import AdyenCheckoutInstance from "../../util/AdyenCheckoutInstance";

export default function ApplePay({ eventRegistration, emitResponse}) {

   const [applePayToken, setApplePayToken] = useState('');

   const [isWasInit, setIsWasInit] = useState(false);


   const { onPaymentProcessing } = eventRegistration;


   function initAdyen() {

      AdyenCheckoutInstance().then(function(response){

         const woosa = adn_util;
         var total =  (woosa.cart.total) * 100; //in cents

         var component = response.create("applepay", {
            amount: {
               currency: woosa.currency,
               value: total.toFixed(), // try in a console JS, there a bug : 68.60 * 100 = 6859.999999999999
            },
            countryCode: woosa.cart.country,
            onAuthorized: (callBackSuccess,callBackError,ApplePayPaymentAutorizedEvent) => {

               if( ! ApplePayPaymentAutorizedEvent.payment.token.paymentData){
                  callBackError()
               }else{

                  var token = JSON.stringify(ApplePayPaymentAutorizedEvent.payment.token.paymentData);

                  setApplePayToken(token);

                  callBackSuccess();

                  document.dispatchEvent( new CustomEvent('WoosaApplePayPaymentAutorizedEvent', {'detail': {ApplePayPaymentAutorizedEvent : ApplePayPaymentAutorizedEvent} }));
               }
            }
         });

         component.isAvailable().then(() => {

            component.mount("#adn-applepay-container");
            setIsWasInit(true);

         }).catch(e => {
            console.log(e)
         });
      });

   }

   useEffect( () => {
      const unsubscribe = onPaymentProcessing( async () => {

         if ( !!applePayToken ) {

            return {
               type: emitResponse.responseTypes.SUCCESS,
               meta: {
                  paymentMethodData: {
                     "woosa_adyen_apple_pay_token": applePayToken
                  },
               },
            };
         }

         return {
            type: emitResponse.responseTypes.ERROR,
            message: 'There was an error',
         };
      } );


      if (!isWasInit) {
         initAdyen();
      }

      return () => {
         unsubscribe();
      };
   }, [
      emitResponse.responseTypes.ERROR,
      emitResponse.responseTypes.SUCCESS,
      onPaymentProcessing,
      applePayToken
   ] );

   return (

      <div id="adn-applepay-container"></div>

   );

}
