import Label from "../../components/Label";
import WebComponentGeneric from '../../components/WebComponentGeneric';
import PaymentAction from "../../components/PaymentAction";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_ideal_data', {
   icon: false,
   payment_action: false
} );

const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - iDEAL', 'integration-adyen-woocommerce' );

// should be not in content to run for non active payment methods
const LabelPaymentAction = () => {
   return <>
      {(!!settings.payment_action && Object.keys(settings.payment_action).length > 0) &&
         <PaymentAction
            action={settings.payment_action}
         />
      }
   </>;
};

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <WebComponentGeneric
         paymentMethod={'ideal'}
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
         generatePaymentMethodData={(data) => ({
            "adn_blik_issuer": data.paymentMethod ? data.paymentMethod.issuer : '',
         })}
      />
   </>;
};

const config = {
   name: 'woosa_adyen_ideal',
   label: <><Label text={title} image={settings.icon} name={'woosa_adyen_ideal'} /><LabelPaymentAction /></>,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};

window.wc.wcBlocksRegistry.registerPaymentMethod( config );