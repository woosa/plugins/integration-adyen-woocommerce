import Label from "../../components/Label";
import CardForm from './components/CardForm';
import PaymentAction from "../../components/PaymentAction";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_bancontact_data', {
   icon: false,
   payment_action: false
} );

const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - Bancontact', 'integration-adyen-woocommerce' );

const LabelPaymentAction = () => {
   return <>
      {(!!settings.payment_action && Object.keys(settings.payment_action).length > 0) &&
         <PaymentAction
            action={settings.payment_action}
         />
      }
   </>;
};

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <CardForm
         installments={settings.get_installments}
         stored_cards={settings.stored_cards}
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
      />
   </>;
};

const config = {
   name: 'woosa_adyen_bancontact',
   label: <><Label text={title} image={settings.icon} name={'woosa_adyen_bancontact'} /><LabelPaymentAction /></>,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};

window.wc.wcBlocksRegistry.registerPaymentMethod( config );