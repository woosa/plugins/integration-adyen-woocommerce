import { useEffect, useState } from 'react';
import AdyenCheckoutInstance from '../../../util/AdyenCheckoutInstance'

export default function Card({card, index, setEncryptedCardData, setActiveIndex, activeIndex, setIsStoredCard}) {

   const [show, setShow] = useState(false);
   const [isInitCard, setIsInitCard] = useState(false);


   useEffect(() => {

      if (isInitCard) {

         AdyenCheckoutInstance(setEncryptedCardData).then(function(response){

            const storedMethods = response.paymentMethodsResponse.storedPaymentMethods;

            response.create('bcmc', storedMethods[index]).mount('#adn-bcmc-card-'+index);

            setShow(true);

            setIsStoredCard(true);

         });

         setIsInitCard(false);


      }

   });

   function initCard() {

      setActiveIndex(index);

      setIsInitCard(true);

   }

   function isShow() {

      if (-1 === activeIndex) {
         return false;
      }

      return show;

   }

   return (

      <div className="adn-stored-card is-stored-card" onClick={initCard}>
         <div className="adn-stored-card__details" data-adn-stored-card={'adn-bcmc-card-' + index} data-adn-stored-card-type="bcmc">
            <img src={'https://checkoutshopper-test.adyen.com/checkoutshopper/images/logos/' + card['brand'] +'.svg'} alt="" />
            <div>******{card['lastFour']}</div>
         </div>
         <div className="adn-stored-card__fields" style={{display: isShow() ? "block" : "none"}} id={'adn-bcmc-card-'+index}></div>
      </div>

   );

}