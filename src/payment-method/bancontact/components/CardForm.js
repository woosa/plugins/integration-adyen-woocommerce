import { useEffect, useState } from 'react';
import StoredCard from "./StoredCard";

export default function CardForm({ installments, stored_cards, eventRegistration, emitResponse }) {

   const [encryptedCardData, setEncryptedCardData] = useState({
      encryptedCardNumber: '',
      encryptedExpiryMonth: '',
      encryptedExpiryYear: '',
      encryptedSecurityCode: '',
      holderName: '',
      storedPaymentMethodId: '',
      store_card: '',
      installments: ''
   });

   const [isStoredCard, setIsStoredCard] = useState(false);

   const { onPaymentProcessing } = eventRegistration;

   useEffect( () => {
      const unsubscribe = onPaymentProcessing( async () => {
         const customDataIsValid = !! encryptedCardData;

         if ( customDataIsValid ) {
            return {
               type: emitResponse.responseTypes.SUCCESS,
               meta: {
                  paymentMethodData: {
                     "adn-bcmc-card-number": encryptedCardData.encryptedCardNumber,
                     "adn-bcmc-card-exp-month": encryptedCardData.encryptedExpiryMonth,
                     "adn-bcmc-card-exp-year": encryptedCardData.encryptedExpiryYear,
                     "adn-bcmc-card-cvc": encryptedCardData.encryptedSecurityCode,
                     "adn-bcmc-card-holder": encryptedCardData.holderName,
                     "adn-bcmc-sci": encryptedCardData.storedPaymentMethodId,
                     "adn-bcmc-store-card": encryptedCardData.store_card,
                     "adn-bcmc-is-stored-card": isStoredCard ? "yes" : "no",
                     "adn-bcmc-card-installments": encryptedCardData.installments,
                  },
               },
            };
         }

         return {
            type: emitResponse.responseTypes.ERROR,
            message: 'There was an error',
         };
      } );

      return () => {
         unsubscribe();
      };
   }, [
      emitResponse.responseTypes.ERROR,
      emitResponse.responseTypes.SUCCESS,
      onPaymentProcessing,
      encryptedCardData
   ] );

   return (
      <div className="adn-wrap-form">

         <StoredCard setEncryptedCardData={setEncryptedCardData} stored_cards={stored_cards} installments={installments} setIsStoredCard={setIsStoredCard}/>

         <input type="hidden" id="adn-bcmc-card-number" name="adn-bcmc-card-number" value={encryptedCardData.encryptedCardNumber} />
         <input type="hidden" id="adn-bcmc-card-exp-month" name="adn-bcmc-card-exp-month" value={encryptedCardData.encryptedExpiryMonth} />
         <input type="hidden" id="adn-bcmc-card-exp-year" name="adn-bcmc-card-exp-year" value={encryptedCardData.encryptedExpiryYear} />
         <input type="hidden" id="adn-bcmc-card-cvc" name="adn-bcmc-card-cvc" value={encryptedCardData.encryptedSecurityCode} />

         <input type="hidden" id="adn-bcmc-card-holder" name="adn-bcmc-card-holder" value={encryptedCardData.holderName} />
         <input type="hidden" id="adn-bcmc-sci" name="adn-bcmc-sci" value={encryptedCardData.storedPaymentMethodId} />
         <input type="hidden" id="adn-bcmc-store-card" name="adn-bcmc-store-card"  value={encryptedCardData.store_card}  />
         <input type="hidden" id="adn-bcmc-is-stored-card" name="adn-bcmc-is-stored-card" value="yes" />
         <input type="hidden" id="adn-bcmc-card-installments" name="adn-bcmc-card-installments" value={encryptedCardData.store_card} />

         <input type="hidden" data-adn-card-installments="" value={installments} />

      </div>
   );
}