import { useEffect, useState } from 'react';

export default function Label({ text, image, name }) {

   const [labelUrl, setLabelUrl] = useState('');
   const [labelTitle, setLabelTitle] = useState('');
   const [isStartFetching, setIsStartFetching] = useState(false);


   useEffect( () => {
      const woosa = adn_util;

      if (!!labelUrl) {
         return;
      }

      if (isStartFetching) {
         return;
      }

      if (!image) {

         setIsStartFetching(true);

         jQuery.ajax({
            url   : woosa.ajax.url,
            method: 'POST',
            data: {
               action     : 'adn_get_payment_method_data',
               security   : woosa.ajax.nonce,
               payment_method: name
            },
            success: function(res) {

               if(!!res.data.payment_method_data.icon){
                  setLabelUrl(res.data.payment_method_data.icon);
               }
               if(!!res.data.payment_method_data.title){
                  setLabelTitle(res.data.payment_method_data.title);
               }

               setIsStartFetching(false);

            }
         });

      }

   });

   return (
      <span style={{ width: '100%' }}>
         {
            labelTitle
               ? labelTitle
               : text
         }
         {
            labelUrl
            ? <img src={labelUrl} style={{ float: 'right', marginRight: '20px' }} />
            : image ? <img src={image} style={{ float: 'right', marginRight: '20px' }} /> : ''
         }
     </span>
   )
}
