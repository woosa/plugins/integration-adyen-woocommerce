import { useEffect, useState } from 'react';
import AdyenCheckoutInstance from "../util/AdyenCheckoutInstance";

export default function WebComponent({
   paymentMethod,
   eventRegistration,
   emitResponse,
   generatePaymentMethodData
 }) {

   const [encryptedCardData, setEncryptedCardData] = useState({});
   const [isWasInit, setIsWasInit] = useState(false);
   const { onPaymentProcessing } = eventRegistration;

   function initAdyen() {
      AdyenCheckoutInstance(setEncryptedCardData).then(function(response){
         response.create(paymentMethod).mount(`#adn-${paymentMethod}-container`);
         setIsWasInit(true);
      });
   }

   useEffect(() => {
      const unsubscribe = onPaymentProcessing(async () => {
         const customDataIsValid = !!encryptedCardData;

         if (customDataIsValid) {
            const paymentMethodData = generatePaymentMethodData
              ? generatePaymentMethodData(encryptedCardData, paymentMethod)
              : {}; // Default to an empty object if no generator is provided.

            return {
               type: emitResponse.responseTypes.SUCCESS,
               meta: { paymentMethodData },
            };
         }

         return {
            type: emitResponse.responseTypes.ERROR,
            message: 'There was an error',
         };
      });

      if (!isWasInit) {
         initAdyen();
      }

      return () => {
         unsubscribe();
      };
   }, [
      emitResponse.responseTypes.ERROR,
      emitResponse.responseTypes.SUCCESS,
      onPaymentProcessing,
      encryptedCardData,
      generatePaymentMethodData
   ]);

   return (
      <div id={`adn-${paymentMethod}-container`}></div>
   );
}