import { useEffect, useState } from 'react';
import AdyenCheckoutInstance from "../util/AdyenCheckoutInstance";

export default function PaymentAction({action }) {

   const [isWasInit, setIsWasInit] = useState(false);
   const [isProcessing, setIsProcessing] = useState(true);

   function initAdyen() {

      AdyenCheckoutInstance().then(function(response){

         response.createFromAction(action).mount('#adn-payment-action-data');

         setIsWasInit(true);
         setIsProcessing(false);

      });

   }

   useEffect( () => {

      if (!isWasInit) {

         initAdyen();

      }

   });

   return (

      <div className="adn-popup">
         <div>
            <div id="adn-payment-action-data" className="adn-component">
               <div className="adn-component__text" style={{display: isProcessing ? "block" : "none"}}>
                  Processing...
               </div>
            </div>
         </div>
      </div>

   );
}