const path = require( 'path' );
const defaultConfig = require( '@wordpress/scripts/config/webpack.config' );
const WooCommerceDependencyExtractionWebpackPlugin = require( '@woocommerce/dependency-extraction-webpack-plugin' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );

// Remove SASS rule from the default config so we can define our own.
const defaultRules = defaultConfig.module.rules.filter( ( rule ) => {
   return String( rule.test ) !== String( /\.(sc|sa)ss$/ );
} );

const paymentMethods = [
   'alipay',
   'apple-pay',
   'bancontact',
   'bancontact-mobile',
   'blik',
   'boleto',
   'card',
   'gift-card',
   'google-pay',
   'grabpay-my',
   'grabpay-ph',
   'grabpay-sg',
   'ideal',
   'klarna',
   'klarna-account',
   'klarna-paynow',
   'mobilepay',
   'molpay-ml',
   'molpay-th',
   'online-banking-poland',
   'paypal',
   'swish',
   'trustly',
   'vipps',
   'wechat-pay',
   'twint',
];

function buildEntry(items) {
   const basePath = path.resolve(process.cwd(), 'src', 'payment-method');
   return items.reduce((entries, block) => {
      entries[block] = path.join(basePath, block, 'index.js');
      return entries;
   }, {});
}

module.exports = {
   ...defaultConfig,
   entry: buildEntry(paymentMethods),
   output: {
      ...defaultConfig.output,
      path: path.resolve(process.cwd(), 'includes/payment-method/assets'),
   },
   module: {
      ...defaultConfig.module,
      rules: [
         ...defaultRules,
         {
            test: /\.(sc|sa)ss$/,
            exclude: /node_modules/,
            use: [
               MiniCssExtractPlugin.loader,
               { loader: 'css-loader', options: { importLoaders: 1 } },
               {
                  loader: 'sass-loader',
                  options: {
                     sassOptions: {
                        includePaths: [ 'src/css' ],
                     },
                  },
               },
            ],
         },
      ],
   },
   plugins: [
      ...defaultConfig.plugins.filter(
         ( plugin ) =>
            plugin.constructor.name !== 'DependencyExtractionWebpackPlugin'
      ),
      new WooCommerceDependencyExtractionWebpackPlugin(),
      new MiniCssExtractPlugin( {
         filename: `[name].css`,
      } ),
   ],
};
