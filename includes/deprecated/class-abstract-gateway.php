<?php
/**
 * Abstract Gateway
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;


class Abstract_Gateway extends Payment_Gateway_Abstract{

   public function get_default_title(){
      return parent::get_default_title();
   }

   public function get_default_description(){
      return parent::get_default_description();
   }

   public function payment_method_type(){
      return parent::payment_method_type();
   }

   public function get_settings_description(){
      return parent::get_settings_description();
   }

   public function recurring_payment_method(){
      return parent::recurring_payment_method();
   }
}