<?php
/**
 * Payment_Gateway_Swish
 *
 * Payment type     : Wallet
 * Payment flow     : QR code or redirect
 * Countries        : SE
 * Currencies       : SEK
 * Recurring        : No
 * Refunds          : Yes
 * Partial refunds  : Yes
 * Separate captures: No
 * Chargebacks      : No
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
use Automattic\WooCommerce\StoreApi\Utilities\NoticeHandler;

defined( 'ABSPATH' ) || exit;


class Payment_Gateway_Swish extends Payment_Gateway_Abstract{


   /**
    * List of countries where is available.
    *
    * @return array
    */
   public function available_countries(){

      return [
         'SE' => [
            'currencies' => ['SEK'],
         ],
      ];
   }



   /**
    * Gets default payment method title.
    *
    * @return string
    */
   public function get_default_title(){
      return __('Adyen - Swish', 'integration-adyen-woocommerce');
   }



   /**
    * Gets default payment method description.
    *
    * @return string
    */
   public function get_default_description(){
      return sprintf(__('Shoppers can pay with Swish when shopping online or in-store using our terminals. %s', 'integration-adyen-woocommerce'), '<br/>'.$this->show_supported_country());
   }



   /**
    * Gets default description set in settings.
    *
    * @return void
    */
   public function get_settings_description(){}



   /**
    * Type of the payment method (e.g ideal, scheme. bcmc).
    *
    * @return string
    */
   public function payment_method_type(){
      return 'swish';
   }



   /**
    * Returns the payment method to be used for recurring payments
    *
    * @return void
    */
   public function recurring_payment_method(){}



   /**
    * Processes the payment.
    *
    * @param int $order_id
    * @return array
    */
   public function process_payment($order_id) {

      parent::process_payment($order_id);

      $order     = wc_get_order($order_id);
      $reference = $order_id;
      $payload   = $this->build_payment_payload( $order, $reference );

      try{

         //make a payment with the gift card if available
         Payment_Gateway_Gift_Card::send_partial_payment($order, $payload);

         $order_data = $order->get_meta('_' . PREFIX . '_order_data');

         if(isset($order_data['remainingAmount'])){
            //pay only the remaining order amount
            $payload['amount'] = $order_data['remainingAmount'];
            $payload['order'] = [
               'pspReference' => $order_data['pspReference'],
               'orderData' => $order_data['orderData'],
            ];
         }

      }catch(\Exception $e){

         wc_add_notice($e->getMessage(), 'error');

         NoticeHandler::convert_notices_to_exceptions( 'woocommerce_rest_payment_error' );

         return ['result' => 'failure'];
      }

      $response = $this->api->checkout()->send_payment($payload);

      if($response->status == 200){

         return $this->process_payment_result( $response, $order );

      }else{

         wc_add_notice($response->body->message, 'error');

         NoticeHandler::convert_notices_to_exceptions( 'woocommerce_rest_payment_error' );

      }

      return ['result' => 'failure'];

   }



   /**
    * Processes the payment result.
    *
    * @param object $response
    * @param \WC_Order $order
    * @return array
    */
   protected function process_payment_result( $response, $order ){

      $body        = Util::obj_to_arr($response->body);
      $result_code = Util::array($body)->get('resultCode');
      $action      = Util::array($body)->get('action');

      $result = [
         'result'   => 'success',
         'redirect' => Service_Util::get_return_page_url($order, $result_code)
      ];

      $order->update_meta_data('_' . PREFIX . '_payment_resultCode', $result_code);
      $order->update_meta_data('_' . PREFIX . '_payment_action', $action);
      $order->save();

      if( 'Pending' == $result_code ){

         //redirect to process payment action via Web Component
         $result = [
            'result'   => 'success',
            'redirect' => add_query_arg(
               [
                  PREFIX . '_payment_method' => $this->payment_method_type(),
                  PREFIX . '_order_id'       => $order->get_id(),
               ],
               Service_Util::get_checkout_url($order)
            )
         ];

      }

      return $result;

   }

}