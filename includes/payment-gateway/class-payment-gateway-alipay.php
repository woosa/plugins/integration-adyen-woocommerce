<?php
/**
 * Payment Gateway Alipay
 *
 * Payment type     : Wallet
 * Payment flow     : Redirect
 * Countries        : CN
 * Currencies       : AUD, CAD, CHF, CNY, CZK, DKK, EUR, GBP, HKD, JPY, MYR, NOK, NZD, RUB, SEK, SGD, THB, USD
 * Recurring        : No
 * Refunds          : Yes
 * Partial refunds  : Yes
 * Separate captures: No
 * Chargebacks      : No
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
use Automattic\WooCommerce\StoreApi\Utilities\NoticeHandler;

defined( 'ABSPATH' ) || exit;


class Payment_Gateway_Alipay extends Payment_Gateway_Ideal{


   /**
    * Constructor of this class.
    *
    * @param bool $init_hooks
    */
   public function __construct($init_hooks = true){
      Payment_Gateway_Abstract::__construct($init_hooks);
   }


   /**
    * List of countries where is available.
    *
    * @return array
    */
   public function available_countries(){

      return [
         'CN' => [
            'currencies' => ['AUD', 'CAD', 'CHF', 'CNY', 'CZK', 'DKK', 'EUR', 'GBP', 'HKD', 'JPY', 'MYR', 'NOK', 'NZD', 'RUB', 'SEK', 'SGD', 'THB', 'USD'],
         ],
      ];
   }



   /**
    * Gets default payment method title.
    *
    * @return string
    */
   public function get_default_title(){
      return __('Adyen - Alipay', 'integration-adyen-woocommerce');
   }



   /**
    * Gets default payment method description.
    *
    * @return string
    */
   public function get_default_description(){
      return $this->show_supported_country();
   }



   /**
    * Type of the payment method (e.g ideal, scheme. bcmc).
    *
    * @return string
    */
   public function payment_method_type(){
      return 'alipay';
   }


   /**
    * Returns the payment method to be used for recurring payments
    *
    * @return void
    */
   public function recurring_payment_method(){}



   /**
    * Validates extra added fields.
    *
    * @return bool
    */
   public function validate_fields() {
      return Payment_Gateway_Abstract::validate_fields();
   }



   /**
    * Builds the required payment payload
    *
    * @param \WC_Order $order
    * @param string $reference
    * @return array
    */
   protected function build_payment_payload(\WC_Order $order, $reference){
      return Payment_Gateway_Abstract::build_payment_payload($order, $reference);
   }



   /**
    * Processes the payment.
    *
    * @param int $order_id
    * @return array
    */
   public function process_payment($order_id) {

      Payment_Gateway_Abstract::process_payment($order_id);

      $order     = wc_get_order($order_id);
      $reference = $order_id;
      $payload   = $this->build_payment_payload($order, $reference);

      try{

         //make a payment with the gift card if available
         Payment_Gateway_Gift_Card::send_partial_payment($order, $payload);

         $order_data = $order->get_meta('_' . PREFIX . '_order_data');

         if(isset($order_data['remainingAmount'])){
            //pay only the remaining order amount
            $payload['amount'] = $order_data['remainingAmount'];
            $payload['order'] = [
               'pspReference' => $order_data['pspReference'],
               'orderData' => $order_data['orderData'],
            ];
         }

      }catch(\Exception $e){

         wc_add_notice($e->getMessage(), 'error');

         NoticeHandler::convert_notices_to_exceptions( 'woocommerce_rest_payment_error' );

         return ['result' => 'failure'];
      }

      $response = $this->api->checkout()->send_payment($payload);

      if($response->status == 200){

         return $this->process_payment_result( $response, $order );

      }else{

         wc_add_notice($response->body->message, 'error');

         NoticeHandler::convert_notices_to_exceptions( 'woocommerce_rest_payment_error' );

      }

      return ['result' => 'failure'];

   }

}