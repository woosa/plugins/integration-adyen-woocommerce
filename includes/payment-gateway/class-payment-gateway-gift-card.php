<?php
/**
 * Payment Gateway Gift Card
 *
 * Payment type     : Loyalty program
 * Payment flow     : Direct
 * Countries        : International
 * Currencies       : Multiple
 * Recurring        : No
 * Refunds          : Yes
 * Partial refunds  : Yes
 * Separate captures: Yes
 * Partial captures : No
 * Chargebacks      : No
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;

use Automattic\WooCommerce\StoreApi\Utilities\NoticeHandler;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Payment_Gateway_Gift_Card extends Payment_Gateway_Abstract{


   /**
    * Constructor of this class.
    *
    * @param bool $init_hooks
    */
   public function __construct($init_hooks = true){

      parent::__construct($init_hooks);

      $this->has_fields = true;

   }


   /**
    * Gets default payment method title.
    *
    * @return string
    */
   public function get_default_title(){
      return __('Adyen - Gift Card', 'integration-adyen-woocommerce');
   }



   /**
    * Gets default payment method description.
    *
    * @return string
    */
   public function get_default_description(){
      return $this->show_supported_country();
   }



   /**
    * Gets default description set in settings.
    *
    * @return void
    */
   public function get_settings_description(){}



   /**
    * Type of the payment method (e.g ideal, scheme. bcmc).
    *
    * @return string
    */
   public function payment_method_type(){
      return 'giftcard';
   }



   /**
    * Returns the payment method to be used for recurring payments
    *
    * @return void
    */
   public function recurring_payment_method(){}



   /**
    * Adds extra fields.
    *
    * @return void
    */
    public function payment_fields() {

      parent::payment_fields();

      echo $this->generate_extra_fields_html();
   }



   /**
    * Generates extra fields HTML.
    *
    * @return void
    */
   public function generate_extra_fields_html(){
      ?>
      <div id="<?php echo esc_attr(PREFIX . '-' . $this->payment_method_type() . '-container');?>"></div>
      <input type="hidden" id="<?php echo esc_attr(Util::prefix($this->payment_method_type() . '_brand'));?>" name="<?php echo esc_attr(Util::prefix($this->payment_method_type() . '_brand'));?>">
      <input type="hidden" id="<?php echo esc_attr(Util::prefix($this->payment_method_type() . '_number'));?>" name="<?php echo esc_attr(Util::prefix($this->payment_method_type() . '_number'));?>">
      <input type="hidden" id="<?php echo esc_attr(Util::prefix($this->payment_method_type() . '_cvc'));?>" name="<?php echo esc_attr(Util::prefix($this->payment_method_type() . '_cvc'));?>">
      <?php
   }



   /**
    * Validates extra added fields.
    *
    * @return bool
    */
   public function validate_fields() {

      $is_valid = parent::validate_fields();
      $number   = Util::array($_POST)->get( Util::prefix($this->payment_method_type() . '_number') );
      $cvc      = Util::array($_POST)->get( Util::prefix($this->payment_method_type() . '_cvc') );

      if(empty($number) || empty($cvc)){
         $is_valid = false;
         wc_add_notice(__('Please select provide the Gift Card number and its security code (CVC).', 'integration-adyen-woocommerce'), 'error');
      }

      return $is_valid;
   }



   /**
    * Builds the required payment payload
    *
    * @param \WC_Order $order
    * @param string $reference
    * @return array
    */
   protected function build_payment_payload(\WC_Order $order, $reference){

      $brand  = Util::array($_POST)->get( Util::prefix($this->payment_method_type() . '_brand') );
      $number = Util::array($_POST)->get( Util::prefix($this->payment_method_type() . '_number') );
      $cvc    = Util::array($_POST)->get( Util::prefix($this->payment_method_type() . '_cvc') );

      $payload = parent::build_payment_payload($order, $reference);

      $payload['paymentMethod']['brand'] = $brand;
      $payload['paymentMethod']['encryptedCardNumber'] = $number;
      $payload['paymentMethod']['encryptedSecurityCode'] = $cvc;

      return $payload;
   }



   /**
    * Processes the payment.
    *
    * @param int $order_id
    * @return array
    */
   public function process_payment($order_id) {

      parent::process_payment($order_id);

      $order     = wc_get_order($order_id);
      $reference = $order_id;
      $payload   = $this->build_payment_payload( $order, $reference );

      $cb_response = $this->api->checkout()->check_gift_card_balance([
         'paymentMethod' => [
            'type' => $payload['paymentMethod']['type'],
            'brand' => $payload['paymentMethod']['brand'],
            'encryptedCardNumber' => $payload['paymentMethod']['encryptedCardNumber'],
            'encryptedSecurityCode' => $payload['paymentMethod']['encryptedSecurityCode'],
         ],
         'amount' => $payload['amount'],
         'merchantAccount' => $payload['merchantAccount'],
      ]);

      if($cb_response->status == 200){

         switch($cb_response->body->resultCode){

            case 'Success':

               $response = $this->api->checkout()->send_payment($payload);

               if($response->status == 200){

                  return $this->process_payment_result($response, $order);

               }else{

                  wc_add_notice($response->body->message, 'error');

                  NoticeHandler::convert_notices_to_exceptions( 'woocommerce_rest_payment_error' );

               }

               break;

            case 'NotEnoughBalance':

               $order_data = $order->get_meta('_' . PREFIX . '_order_data');

               if(time() > strtotime(Util::array($order_data)->get('expiresAt', 0))){

                  $co_response = $this->api->checkout()->create_order([
                     'reference'       => $order_id . '_' . Util::random_string(),
                     'amount'          => $payload['amount'],
                     'merchantAccount' => $payload['merchantAccount'],
                  ]);

                  if($co_response->status != 200){
                     return ['result' => 'failure'];
                  }

                  $order->update_meta_data('_' . PREFIX . '_order_data', [
                     'pspReference'    => $co_response->body->pspReference,
                     'orderData'       => $co_response->body->orderData,
                     'expiresAt'       => $co_response->body->expiresAt,
                     'remainingAmount' => Util::obj_to_arr($co_response->body->remainingAmount),
                  ]);
                  $order->update_meta_data('_' . PREFIX . '_giftcard_data', [
                     'balance'               => Util::obj_to_arr($cb_response->body->balance),
                     'brand'                 => $payload['paymentMethod']['brand'],
                     'encryptedCardNumber'   => $payload['paymentMethod']['encryptedCardNumber'],
                     'encryptedSecurityCode' => $payload['paymentMethod']['encryptedSecurityCode'],
                  ]);
                  $order->save_meta_data();
               }

               return [
                  'result'   => 'success',
                  'redirect' => add_query_arg(
                     [
                        PREFIX . '_giftcard_requirement' => wp_create_nonce('select_payment_method_for_giftcard'),
                     ],
                     $order->get_checkout_payment_url()
                  )
               ];

               break;
         }

      }else{

         wc_add_notice(sprintf(__('Checking the gift card balance has failed due to: %s', 'integration-adyen-woocommerce'), $cb_response->body->message), 'error');

         NoticeHandler::convert_notices_to_exceptions( 'woocommerce_rest_payment_error' );
      }

      return ['result' => 'failure'];
   }



   /**
    * Processes the payment result.
    *
    * @param object $response
    * @param \WC_Order $order
    * @return array
    */
   protected function process_payment_result( $response, $order ){

      $body        = Util::obj_to_arr($response->body);
      $result_code = Util::array($body)->get('resultCode');
      $reference   = Util::array($body)->get('pspReference');
      $error_codes = ['Refused', 'Error', 'Cancelled'];

      $result = [
         'result'   => 'success',
         'redirect' => Service_Util::get_return_page_url($order, $result_code)
      ];

      $order->update_meta_data('_' . PREFIX . '_payment_resultCode', $result_code);
      $order->update_meta_data('_' . PREFIX . '_payment_pspReference', $reference);
      $order->save_meta_data();

      if(in_array($result_code, $error_codes)){

         wc_add_notice(__('The transaction via Gift Card could not be processed.', 'integration-adyen-woocommerce'), 'error');

         NoticeHandler::convert_notices_to_exceptions( 'woocommerce_rest_payment_error' );

         $result = [
            'result' => 'failure'
         ];

      }

      return $result;
   }



   /**
    * Makes a partial payment.
    *
    * @param \WC_Order $order
    * @param array $payload
    * @throws \Exception
    * @return bool
    */
   public static function send_partial_payment(\WC_Order $order, array $payload){

      $error_codes = ['Refused', 'Error', 'Cancelled'];
      $card_data   = $order->get_meta('_' . PREFIX . '_giftcard_data');
      $order_data  = $order->get_meta('_' . PREFIX . '_order_data');

      if( ! empty($card_data['already_used']) || empty($card_data['encryptedCardNumber']) || empty($card_data['encryptedSecurityCode']) ){
         return false;
      }

      $response = Service::checkout()->send_payment([
         'reference'       => $payload['reference'],
         'merchantAccount' => $payload['merchantAccount'],
         'paymentMethod'   => [
            'type'                  => 'giftcard',
            'brand'                 => $card_data['brand'],
            'encryptedCardNumber'   => $card_data['encryptedCardNumber'],
            'encryptedSecurityCode' => $card_data['encryptedSecurityCode'],
         ],
         'amount'          => [
            'currency' => $payload['amount']['currency'],
            'value'    => $card_data['balance']['value']
         ],
         'order'           => [
            'pspReference' => $order_data['pspReference'],
            'orderData'    => $order_data['orderData'],
         ],
      ]);

      if($response->status == 200){

         if(in_array($response->body->resultCode, $error_codes)){
            throw new \Exception('Processing the gift card payment has failed due to: ' .json_encode($response->body));
         }

         $card_data['already_used']     = true;
         $order_data['orderData']       = Util::obj_to_arr($response->body->order->orderData);
         $order_data['remainingAmount'] = Util::obj_to_arr($response->body->order->remainingAmount);

         $order->update_meta_data('_' . PREFIX . '_giftcard_data', $card_data);
         $order->update_meta_data('_' . PREFIX . '_order_data', $order_data);
         $order->update_meta_data('_' . PREFIX . '_payment_by_giftcard', 'partial');
         $order->save_meta_data();

         return true;

      }

      throw new \Exception('Processing the gift card payment has failed due to: ' .json_encode($response->body));
   }

}