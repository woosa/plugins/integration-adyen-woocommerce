<?php
/**
 * Payment Gateway GrabPay - Malaysia
 *
 * Payment type     : Wallet|PayLater
 * Payment flow     : Redirect
 * Countries        : MY
 * Currencies       : MYR
 * Recurring        : No
 * Refunds          : Yes
 * Partial refunds  : Yes
 * Separate captures: No
 * Chargebacks      : No
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Payment_Gateway_Grabpay_MY extends Payment_Gateway_Grabpay {


   /**
    * List of countries where is available.
    *
    * @return array
    */
   public function available_countries(){

      return [
         'MY' => [
            'currencies' => ['MYR'],
         ],
      ];
   }



   /**
    * Gets default payment method title.
    *
    * @return string
    */
   public function get_default_title(){
      return __('Adyen - GrabPay - Malaysia', 'integration-adyen-woocommerce');
   }



   /**
    * Type of the payment method (e.g ideal, scheme. bcmc).
    *
    * @return string
    */
   public function payment_method_type(){
      return 'grabpay_MY';
   }


}