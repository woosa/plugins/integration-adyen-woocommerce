<?php
/**
 * Payment Gateway MolPay TH - online banking
 *
 * Payment type     : Online banking
 * Payment flow     : Redirect
 * Countries        : TH
 * Currencies       : THB
 * Recurring        : No
 * Refunds          : Yes
 * Partial refunds  : Yes
 * Separate captures: No
 * Chargebacks      : No
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Payment_Gateway_MolPay_TH extends Payment_Gateway_MolPay{



   /**
    * List of countries where is available.
    *
    * @return array
    */
   public function available_countries(){

      return [
         'TH' => [
            'currencies' => ['THB'],
         ],
      ];
   }



   /**
    * Gets default payment method title.
    *
    * @return string
    */
   public function get_default_title(){
      return __('Adyen - MOLPay - Thailand', 'integration-adyen-woocommerce');
   }





   /**
    * Type of the payment method (e.g ideal, scheme. bcmc).
    *
    * @return string
    */
   public function payment_method_type(){
      return 'molpay_ebanking_TH';
   }

}