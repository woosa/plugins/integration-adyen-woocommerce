<?php
/**
 * Payment Gateway Klarna PayNow
 *
 * Payment type     : Buy Now Pay Now
 * Payment flow     : Redirect
 * Countries        : AT, DE, SE, CH, NL
 * Currencies       : EUR, SEK, CHF
 * Recurring        : Yes
 * Refunds          : Yes
 * Partial refunds  : Yes
 * Separate captures: Yes
 * Partial captures : Yes
 * Chargebacks      : Yes
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Payment_Gateway_Klarna_PayNow extends Payment_Gateway_Klarna{


   /**
    * List of countries where is available.
    *
    * @return array
    */
   public function available_countries(){

      return [
         'AT' => [
            'currencies' => ['EUR'],
            'recurring' => false,
         ],
         'DE' => [
            'currencies' => ['EUR'],
            'recurring' => true,
         ],
         'SE' => [
            'currencies' => ['SEK'],
            'recurring' => true,
         ],
         'CH' => [
            'currencies' => ['CHF'],
            'recurring' => false,
         ],
         'NL' => [
            'currencies' => ['EUR'],
            'recurring' => true,
         ],
      ];
   }



   /**
    * Gets default payment method title.
    *
    * @return string
    */
   public function get_default_title(){
      return __('Adyen - Klarna - Pay now', 'integration-adyen-woocommerce');
   }



   /**
    * Gets default payment method description.
    *
    * @return string
    */
   public function get_default_description(){

      $output = sprintf(__('Pay the whole amount instantly, either by online banking or direct debit. %s', 'integration-adyen-woocommerce'), '<br/>'.$this->show_supported_country());
      $output .= '<br/>'.$this->show_rec_supported_country();

      return $output;
   }



   /**
    * Type of the payment method (e.g ideal, scheme. bcmc).
    *
    * @return string
    */
   public function payment_method_type(){
      return 'klarna_paynow';
   }


}