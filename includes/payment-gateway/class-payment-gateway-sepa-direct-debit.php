<?php
/**
 * Payment Gateway Sepa Direct Debit
 *
 * This class creates SEPA Direct Debit payment method.
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Payment_Gateway_Sepa_Direct_Debit extends Payment_Gateway_Abstract{


   /**
    * Constructor of this class.
    *
    * @param bool $init_hooks
    */
    public function __construct($init_hooks = true){

      parent::__construct($init_hooks);

      $this->has_fields = true;
      $this->currencies = ['EUR'];
   }



   /**
    * Gets default payment method title.
    *
    * @return string
    */
   public function get_default_title(){
      return __('Adyen - SEPA Direct Debit', 'integration-adyen-woocommerce');
   }



   /**
    * Gets default payment method description.
    *
    * @return string
    */
   public function get_default_description(){
      return __('SEPA Direct Debit is used for recurring payments with WooCommerce Subscriptions, and will not be shown in the WooCommerce checkout.', 'integration-adyen-woocommerce');
   }



   /**
    * Gets default description set in settings.
    *
    * @return void
    */
   public function get_settings_description(){}



   /**
    * Type of the payment method (e.g ideal, scheme. bcmc).
    *
    * @return string
    */
   public function payment_method_type(){
      return 'sepadirectdebit';
   }



   /**
    * Returns the payment method to be used for recurring payments
    *
    * @return string
    */
   public function recurring_payment_method(){
      return $this->payment_method_type();
   }



   /**
	 * Checks if the gateway is available for use.
	 *
	 * @return bool
	 */
	public function is_available() {

      //we do not want to show SEPA in checkout page
      return false;
   }


}