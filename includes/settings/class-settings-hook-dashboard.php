<?php
/**
 * Settings Hook Dashboard
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Settings_Hook_Dashboard implements Interface_Hook{


   /**
    * Initiates the hooks.
    *
    * @return void
    */
   public static function init(){

      add_filter(PREFIX . '\module\settings\page\content\fields\dashboard', [__CLASS__, 'add_section_fields'], 20);
   }



   /**
    * Adds the fields of the section.
    *
    * @param array $fields
    * @return array
    */
   public static function add_section_fields($fields){

      $new_fields = [];

      foreach($fields as $field) {

         $new_fields[] = $field;

         if(Util::prefix('settings_start') === $field['id']) {

            $new_fields[] = [
               'name'     => __('Capture mode', 'integration-adyen-woocommerce'),
               'id'       => PREFIX.'_capture_payment',
               'type'     => 'select',
               'desc' => self::capture_desc(),
               'default' => 'immediate',
               'options' => [
                  'immediate' => __('Immediate', 'integration-adyen-woocommerce'),
                  'delay' => __('With delay', 'integration-adyen-woocommerce'),
                  'manual' => __('Manual', 'integration-adyen-woocommerce'),
               ]
            ];
            $new_fields[] = [
               'name' => __('Reference prefix', 'integration-adyen-woocommerce'),
               'type' => 'text',
               'desc_tip' => __('Specify a prefix (unique per webshop) for the payment reference. NOTE: Use this option only if you have a multisite installation otherwise you can leave it empty.', 'integration-adyen-woocommerce'),
               'id'   => PREFIX .'_order_reference_prefix',
            ];
            $new_fields[] = [
               'id'   => PREFIX .'_auto_klarna_payments',
               'name'  => __('Capture Klarna payments automatically?', 'integration-adyen-woocommerce'),
               'desc' => __('Before enable this you have to contact Adyen support to also set up automatic capture for Klarna payments.', 'integration-adyen-woocommerce'),
               'type'  => 'toggle',
            ];
            $new_fields[] = [
               'name' => __('Remove customer\'s data', 'integration-adyen-woocommerce'),
               'desc' => sprintf(__('This allows your customers to remove their personal data (%s) attached to an order payment. This only deletes the customer-related data for the specific payment, but does not cancel the existing recurring transaction.', 'integration-adyen-woocommerce'), '<a href="https://gdpr-info.eu/art-17-gdpr/" target="_blank">GDPR</a>'),
               'type' => 'toggle','default' => 'no',
               'id'   => PREFIX .'_allow_remove_gdpr',
            ];
            $new_fields[] = [
               'name' => __('Include server port', 'integration-adyen-woocommerce'),
               'desc' => __('Generate the client key for the shop domain by including the server port as well.', 'integration-adyen-woocommerce'),
               'type' => 'toggle',
               'default' => 'yes',
               'id'   => PREFIX .'_incl_server_port',
            ];

         }
      }

      return $new_fields;
   }



   /**
    * Displays the description for `Capture mode` option.
    *
    * @since 1.0.0
    * @return void
    */
   public static function capture_desc(){

      ob_start();
      ?>

      <p class="description"><?php _e('NOTE: you have to enable this option in Adyen account as well!', 'integration-adyen-woocommerce');?></p>
      <p class="description"><?php _e('Manual: you need to explicitly request a capture for each payment.', 'integration-adyen-woocommerce');?></p>

      <?php

      $output = str_replace(array("\r","\n"), '', trim(ob_get_clean()));

      return $output;
   }



   /**
    * Sanitizes the value before saving it.
    *
    * @since 1.1.0
    * @param string $value
    * @return string
    */
   public static function sanitize_order_reference_prefix($value){

      $value = preg_replace('/[^a-zA-Z0-9]/', '', $value);
      $value = strtoupper(substr($value, 0, 4));

      return $value;
   }

}