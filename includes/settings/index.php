<?php
/**
 * Index
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


//init
Settings_Hook::init();
Settings_Hook_Dashboard::init();
Settings_Hook_Payments::init();
Settings_Hook_Stores::init();
Settings_Hook_Webhooks::init();