<?php
/**
 * Core Hook
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;

use Automattic\WooCommerce\Blocks\Payments\PaymentMethodRegistry;
use Automattic\WooCommerce\Utilities\FeaturesUtil;

//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Core_Hook implements Interface_Hook{


   /**
    * Initiates the hooks.
    *
    * @return void
    */
   public static function init(){

      add_action('before_woocommerce_init', [__CLASS__, 'declare_checkout_blocks_compatibility']);
      add_filter('woocommerce_payment_gateways', [__CLASS__, 'define_payment_gateways']);
      add_action('woocommerce_blocks_loaded', [__CLASS__, 'register_payment_methods']);
   }



   /**
    * Declares checkout blocks compatibility.
    *
    * @return void
    */
   public static function declare_checkout_blocks_compatibility() {

      if (class_exists( FeaturesUtil::class )) {
         FeaturesUtil::declare_compatibility('cart_checkout_blocks', DIR_BASENAME, true);
      }
   }



   /**
    * Defines the payment gateways.
    *
    * @param array $gateways
    * @return array
    */
   public static function define_payment_gateways($gateways) {

      $gateways[] = Payment_Gateway_Alipay::class;
      $gateways[] = Payment_Gateway_Apple_Pay::class;
      $gateways[] = Payment_Gateway_Bancontact_Mobile::class;
      $gateways[] = Payment_Gateway_Bancontact::class;
      $gateways[] = Payment_Gateway_Blik::class;
      $gateways[] = Payment_Gateway_Boleto::class;
      $gateways[] = Payment_Gateway_Card::class;
      $gateways[] = Payment_Gateway_Gift_Card::class;
      $gateways[] = Payment_Gateway_Google_Pay::class;
      $gateways[] = Payment_Gateway_Grabpay_MY::class;
      $gateways[] = Payment_Gateway_Grabpay_PH::class;
      $gateways[] = Payment_Gateway_Grabpay_SG::class;
      $gateways[] = Payment_Gateway_Ideal::class;
      $gateways[] = Payment_Gateway_Klarna_Account::class;
      $gateways[] = Payment_Gateway_Klarna_PayNow::class;
      $gateways[] = Payment_Gateway_Klarna::class;
      $gateways[] = Payment_Gateway_Mobilepay::class;
      $gateways[] = Payment_Gateway_MolPay_ML::class;
      $gateways[] = Payment_Gateway_MolPay_TH::class;
      $gateways[] = Payment_Gateway_Online_Banking_Poland::class;
      $gateways[] = Payment_Gateway_Paypal::class;
      $gateways[] = Payment_Gateway_Sepa_Direct_Debit::class;
      $gateways[] = Payment_Gateway_Swish::class;
      $gateways[] = Payment_Gateway_Trustly::class;
      $gateways[] = Payment_Gateway_Vipps::class;
      $gateways[] = Payment_Gateway_Wechat_Pay::class;
      $gateways[] = Payment_Gateway_Twint::class;

      return $gateways;
   }



   /**
    * Registers the payment methods.
    *
    * @return void
    */
   public static function register_payment_methods() {

      if ( ! class_exists( 'Automattic\WooCommerce\Blocks\Payments\Integrations\AbstractPaymentMethodType' ) ) {
         return;
      }

      add_action(
         'woocommerce_blocks_payment_method_type_registration',
         function(PaymentMethodRegistry $payment_methods) {

            $payment_methods->register(new Payment_Method_Alipay);
            $payment_methods->register(new Payment_Method_Apple_Pay);
            $payment_methods->register(new Payment_Method_Bancontact_Mobile);
            $payment_methods->register(new Payment_Method_Bancontact);
            $payment_methods->register(new Payment_Method_Blik);
            $payment_methods->register(new Payment_Method_Boleto);
            $payment_methods->register(new Payment_Method_Card);
            $payment_methods->register(new Payment_Method_Gift_Card);
            $payment_methods->register(new Payment_Method_Google_Pay);
            $payment_methods->register(new Payment_Method_Grabpay_MY);
            $payment_methods->register(new Payment_Method_Grabpay_PH);
            $payment_methods->register(new Payment_Method_Grabpay_SG);
            $payment_methods->register(new Payment_Method_Ideal);
            $payment_methods->register(new Payment_Method_Klarna_Account);
            $payment_methods->register(new Payment_Method_Klarna_PayNow);
            $payment_methods->register(new Payment_Method_Klarna);
            $payment_methods->register(new Payment_Method_Mobilepay);
            $payment_methods->register(new Payment_Method_MOLPay_ML);
            $payment_methods->register(new Payment_Method_MOLPay_TH);
            $payment_methods->register(new Payment_Method_Online_Banking_Poland);
            $payment_methods->register(new Payment_Method_Paypal);
            $payment_methods->register(new Payment_Method_Swish);
            $payment_methods->register(new Payment_Method_Trustly);
            $payment_methods->register(new Payment_Method_Vipps);
            $payment_methods->register(new Payment_Method_Wechat_Pay);
            $payment_methods->register(new Payment_Method_Twint);
         }
      );
   }

}
