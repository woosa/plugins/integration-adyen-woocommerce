<?php
/**
 * Checkout Hook
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Checkout_Hook implements Interface_Hook{


   /**
    * Initiates the hooks.
    *
    * @return void
    */
   public static function init(){

      add_action('woocommerce_checkout_update_order_review', [__CLASS__, 'save_googlepay_fields']);
      add_action('woocommerce_order_details_after_order_table', [__CLASS__, 'display_order_items']);
      add_filter('woocommerce_available_payment_gateways', [__CLASS__, 'display_payment_methods_for_giftcard'], 10, 1);
      add_action('before_woocommerce_pay_form', [__CLASS__, 'display_info_message_for_giftcard'], 20);
   }



   /**
    * Saves custom fields of googlepay payment method in cart session
    *
    * @param string $post_data
    * @return void
    */
   public static function save_googlepay_fields($post_data){

      parse_str($post_data, $payload);

      $token = isset($payload['woosa_adyen_google_pay_token']) ? $payload['woosa_adyen_google_pay_token'] : '';
      $description = isset($payload['woosa_adyen_google_pay_description']) ? $payload['woosa_adyen_google_pay_description'] : '';

      if( ! empty($token) ){
         WC()->session->set( 'woosa_adyen_google_pay_token', $token );
      }

      if( ! empty($description) ){
         WC()->session->set( 'woosa_adyen_google_pay_description', $description );
      }

   }



   /**
    * Displays extra details in customer's order.
    *
    * @param \WC_Order $order
    * @return void
    */
   public static function display_order_items($order) {

      $boleto = new Payment_Gateway_Boleto();

      $boleto->display_order_items($order);

   }



   /**
    * Displays only Adyen payment methods to be selected when the gift card balance does not fully cover the order.
    *
    * In this way the user has to select a payment method to use together with the Gift Card method selected earlier.
    *
    * @param array $available_gateways
    * @return array
    */
   public static function display_payment_methods_for_giftcard($available_gateways){

      if(Checkout::has_gift_card_requirement()){

         foreach ($available_gateways as $gateway_id => $gateway) {
            // Remove payment gateways that don't start with 'woosa_adyen' and the Gift Card as well
            if (strpos($gateway_id, 'woosa_adyen') !== 0 || 'woosa_adyen_gift_card' === $gateway_id) {
               unset($available_gateways[$gateway_id]);
            }
         }
      }

      return $available_gateways;
   }



   /**
    * Displays a message informing the user to select a payment method because the gift card balance does not cover the order.
    *
    * @return void
    */
   public static function display_info_message_for_giftcard(){

      if(Checkout::has_gift_card_requirement()){

         $order_id  = get_query_var('order-pay');
         $order     = wc_get_order($order_id);
         $card_data = $order->get_meta('_' . PREFIX . '_giftcard_data');
         $value     = Util::array($card_data)->get('balance/value') / 100; //the value is in cents

         ?>
         <section class="woocommerce-info">
            <div><?php echo sprintf(__('Your gift card balance is %s which insufficient to cover this order. Please select a payment method to pay the remaining amount.', 'integration-adyen-woocommerce'), '<b>' . wc_price($value, Util::array($card_data)->get('balance/currency')) . '</b>');?></div>
            <div><?php echo sprintf(__('If you want to use a different gift card %sclick here%s.', 'integration-adyen-woocommerce'), '<a href="'.wc_get_checkout_url().'">', '</a>');?></div>
         </section>
         <?php

      }
   }


}
