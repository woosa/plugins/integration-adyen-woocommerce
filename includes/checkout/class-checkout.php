<?php
/**
 * Checkout
 *
 * @author Team Woosa
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Checkout{


   /**
    * Checks if checkout contains at least one subscription.
    *
    * @return bool
    */
   public static function has_subscription(){

      if( WC()->cart ){

         foreach(WC()->cart->get_cart() as $item){
            if( $item['data']->is_type(['subscription_variation', 'subscription'])){
               return true;
            }
         }
      }

      return false;
   }



   /**
    * Checks whether or not the Gift Card requires a second payment method.
    *
    * @return boolean
    */
   public static function has_gift_card_requirement(){

      if( ! is_wc_endpoint_url('order-pay') ) {
         return false;
      }

      $order_id = get_query_var('order-pay');
      $order    = wc_get_order($order_id);

      if( ! $order instanceof \WC_Order ){
         return false;
      }

      $order_data       = $order->get_meta('_' . PREFIX . '_order_data');
      $remaining_to_pay = Util::array($order_data)->get('remainingAmount/value');

      if('pending' !== $order->get_status()){
         return false;
      }

      if( ! wp_verify_nonce(Util::array($_GET)->get(PREFIX . '_giftcard_requirement'), 'select_payment_method_for_giftcard' ) && empty($remaining_to_pay)){
         return false;
      }

      if('woosa_adyen_gift_card' !== $order->get_payment_method() && empty($remaining_to_pay)){
         return false;
      }

      return true;
   }
}